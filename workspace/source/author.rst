.. _author-index:

###########
Author
###########

| **최창원 Choe Changwon(26)**
| \- 010-7314-4993
| \- qwefgh90@naver.com

| \- Software Developer at Korean Aeropsace Research Institude (2015~)
| \- Incheon University CS
| \- Samsung Software Mebership (2014~2015)
| \- Algorithm, Python, Java, Spring, Design Pattern, Data Analysis, Statistics


| **Works**
| `https://github.com/qwefgh90/ <https://github.com/qwefgh90/>`_
| `http://www.slideshare.net/changwonchoe7 <http://www.slideshare.net/changwonchoe7>`_
| `https://www.facebook.com/changwon.choe.7 <https://www.facebook.com/changwon.choe.7>`_
| `https://qwefgh90.github.io <https://qwefgh90.github.io>`_
| `http://yobi.d2fest.kr/qwefgh90 <http://yobi.d2fest.kr/qwefgh90>`_
