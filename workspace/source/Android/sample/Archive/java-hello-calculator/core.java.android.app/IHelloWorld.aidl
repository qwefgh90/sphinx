package android.app;

interface IHelloWorld
{
	void printHello();
	int add(in int numberOne, in int numberTwo);        
    	int subtract(in int numberOne, in int numberTwo);
    	int multiple(in int numberOne, in int numberTwo);
    	int divide(in int numberOne, in int numberTwo);
}
