.. _android_index:

.. meta::
	:site_name: Chang docs
	:title: Android index page
	:description: This is a android index page.
	:keywords: android, framework, activity, service, broadcast, content provider, 안드로이드

Android
==================================================

Contents:

.. toctree::
   :maxdepth: 2
   :numbered: 

   android_kernel.rst
   android_basic.rst
   android_facebook.rst

.. raw:: html
   :file: ../comment_tag1.txt