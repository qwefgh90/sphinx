.. _document-index:

.. meta::
   :site_name: Chang docs
   :title: Changwon docs index page
   :description: This is a Changwon documentation.
   :keywords: Java, Spring, 스프링, 자바, Python, Security, Android, 보안, 안드로이드, 파이썬, 통계, Statistics, 자료구조, 알고리즘, 최창원, 창원, Changwon, Chang, Documentation, docs, Sphinx, Github, site, website, data mining, mathine learning, Incheon, Software, Program, C++, C, Java, 네트워크, TCP/IP, Network, MTU, Fragmentation, Segmentation

IT Wiki
=========================================================

| 안녕하세요. IT 관련 정보공유 및 다시 볼 수 있도록 문서를 작성하고 있습니다.
| 본 스핑크스 문서에서 다루는 내용은 프로그래밍 언어(파이썬,자바 등), 통계학, 보안 입니다. 스핑크스의 소스는 좌측 `Source Code <_sources/index.txt>`_ 로 확인할 수 있습니다.

* :ref:`genindex`
* :ref:`search`

Category:

.. toctree::
   :maxdepth: 2

   author.rst
   python/python_index.rst
   spring/spring_index.rst
   java/java_index.rst
   cpp/cpp_index.rst
   database/database_index.rst
   network/network_index.rst
   os/operating_system_index.rst
   datastructure/datastructure_index.rst
   Android/android_index.rst
   statistics/statistics_index.rst
   security/security_index.rst
   tip/tip_index.rst
   music/music_index.rst
   ict/ict_index.rst

SlideShare
===================

.. raw:: html
   
  <iframe src="http://www.slideshare.net/changwonchoe7/slideshelf" width="615px" height="470px" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:none;" allowfullscreen webkitallowfullscreen mozallowfullscreen></iframe>

.. raw:: html
   :file: comment_tag1.txt
  