.. _database_index:

.. meta::
	:site_name: Chang docs
	:title: Database index page
	:description: This is a Databse index page.
	:keywords: database, db, redis, memory, rdbms, sql, 전문가, sql 전문가, sql 전문가 가이드, 데이터베이스, 쿼리, select, update, insert, delete

데이터베이스
==================================================

Contents:
	
.. toctree::
   :maxdepth: 2
   :numbered: 

   redis_basic.rst
   model_basic.rst
   database_concept.rst
   sql_basic.rst
   sql_basic2.rst
   sql_optimize.rst

.. raw:: html
   :file: ../comment_tag1.txt