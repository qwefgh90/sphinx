.. operating_system_index:


.. meta::
	:site_name: Chang docs
	:title: Operating System index page
	:description: This is a Operating System index page.
	:keywords: OS, Windows, Process, Thread

운영체제와 시스템
==================================================

Contents:
	
.. toctree::
   :maxdepth: 2
   :numbered: 

   operating_system.rst
   distributed_system.rst

.. raw:: html
   :file: ../comment_tag1.txt